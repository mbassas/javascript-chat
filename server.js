const path = require('path');
const express = require('express');

const app = express();
const port = 3001;

const ServerModel = require('./ServerModel');


/* Serve CSS and JS to client application */
app.use(
    '/assets',
    express.static(path.join(__dirname, './client/assets'))
)

const model = new ServerModel();

/* Serve client application */
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, './client/index.html'));
});

/* Starts application */
app.listen(port, function () {
    console.log(`Client chat available here http://localhost:${port}!`)
});
