// Fires initApplication when page loaded
document.addEventListener('DOMContentLoaded', initApplication);
// Close server connection when page closed.
window.onbeforeunload = function() {
    serverConnection.close();
}

const MESSAGE_TYPE = {
    INITIAL_DATA: 'INITIAL_DATA',
    USER_CONNECTED: 'USER_CONNECTED',
    MESSAGE: 'MESSAGE',
    USER_DISCONNECTED: 'USER_DISCONNECTED'
}

var username = "";
var serverConnection;

/* Application init */
function initApplication() {
    setUserName();
    initSendMessageForm();
    initWebsocketConnection();
}
/* ------- */

/* WebSockets */
function initWebsocketConnection() {
    serverConnection = new WebSocket('ws://localhost:45000');
    
    serverConnection.onopen = function () {
        console.log('connected');
        serverConnection.send(createUserConnectedMessage(username))
    };

    serverConnection.onmessage = function (message) {
        const messageData = JSON.parse(message.data);
        switch (messageData.type) {
            case MESSAGE_TYPE.USER_CONNECTED:
                userConnected(messageData.data);
                break;
        
            case MESSAGE_TYPE.MESSAGE:
                writeMessage(messageData.data);
                break;
            case MESSAGE_TYPE.USER_DISCONNECTED:
                userDisconnected(messageData.data);
                break;
            case MESSAGE_TYPE.INITIAL_DATA:
                writeUsers(messageData.data.users);
                writeMessages(messageData.data.messages);
                scrollToBottom();
                break;
            default:
                break;
        }
    }
}
/* ------ */

/* Send message form */
function initSendMessageForm() {
    const sendMessageForm = document.getElementById('send-message-form');
    const messageInput = document.getElementById('message-input');
    sendMessageForm.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopPropagation();

        const message = messageInput.value;

        if(!message) {
            alert("No es pot enviar un missatge buit");
            return;
        }
        
        serverConnection.send(createSendMessage(message));
        writeMessage({username, message});

        messageInput.value = "";
    })    
}
/* ------- */

/* Message creators */
function createUserConnectedMessage(username) {
    return JSON.stringify({
        type: MESSAGE_TYPE.USER_CONNECTED,
        data: username
    });
}

function createSendMessage(message) {
    return JSON.stringify({
        type: MESSAGE_TYPE.MESSAGE,
        data: {
            username,
            message
        }
    });
}
/* --- */

/* Update UI */
function setUserName() {
    do {
        username = prompt('Who are you?', '');
    } while(!username);
    userConnected(username);
}
function writeUsers(users) {
    users.forEach(userConnected);
}
function userConnected(username) {
    document.querySelector('.message-feed').innerHTML += `
        <div class='user_connected'>
            ${username} ha entrat a la conversa
        </div>
    `;

    document.querySelector('.users-list').innerHTML += `
        <div id='user_${username}'>${username}</div>
    `;
    scrollToBottom();
}

function userDisconnected(username) {
    document.getElementById(`user_${username}`).remove();

    document.querySelector('.message-feed').innerHTML += `
        <div class='user_connected'>
            ${username} ha deixat la conversa
        </div>
    `;
    scrollToBottom();
}

function writeMessages(messages) {
    messages.forEach(writeMessage);
}

function writeMessage (messageData) {
    const isMine = username === messageData.username;   
    document.querySelector('.message-feed').innerHTML += `
        <div class='message ${isMine ? 'mine' : ''}'>
            <div class='message_content'>
                <span class='message_author'>
                    ${messageData.username}
                </span>
                <span class='message_body'>
                    ${messageData.message}
                </span>
            </div>
        </div>
    `;

    scrollToBottom();
}

function scrollToBottom() {
    const messageFeed = document.querySelector('.message-feed');
    messageFeed.scrollTop = Number.MAX_SAFE_INTEGER;
}



