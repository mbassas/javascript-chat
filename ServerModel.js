const ws = require('ws');

const MESSAGE_TYPE = {
    INITIAL_DATA: 'INITIAL_DATA',
    USER_CONNECTED: 'USER_CONNECTED',
    MESSAGE: 'MESSAGE',
    USER_DISCONNECTED: 'USER_DISCONNECTED'
}

class ServerModel {

    constructor() {        
        /* Bind context */
        this.onConnection = this.onConnection.bind(this);
        this.broadcast = this.broadcast.bind(this);
        
        
        /* Configure websocket service */
        this.WebSocketServer = new ws.Server({ port: 45000 });
        this.WebSocketServer.on('connection', this.onConnection); 

        /* Initialize structures */
        this.clients = [];
        this.lastMessages = [];
    }

    /**
     * Will fire every time a user connects
     * @param {WebSocket} client 
     */
    onConnection(client) {
        const self = this;

        //JavaScript Object Notation
        client.send(JSON.stringify({
            type: MESSAGE_TYPE.INITIAL_DATA,
            data: {
                users: self.clients.map(c => c.username),
                messages: self.lastMessages
            }
        }));

        var clientObject = {
            username: 'anonymous',
            client
        };

        self.clients.push(clientObject);

        // Fires every time a client sends a message
        client.onmessage = function(message) {
            const messageData = JSON.parse(message.data);
            switch (messageData.type) {
                case MESSAGE_TYPE.USER_CONNECTED:
                    clientObject.username = messageData.data;
                    break;
                case MESSAGE_TYPE.MESSAGE:
                    // Saves this message in the message log
                    self.lastMessages.push(messageData.data);
                    if(self.lastMessages.length > 20) {
                        self.lastMessages.splice(0, 1);
                    }
                    break;
                default:
                    break;
            }
            self.broadcast(client, messageData);
        };

        client.onclose = function () {
            self.broadcast(client, {
                type: MESSAGE_TYPE.USER_DISCONNECTED,
                data: clientObject.username
            });

            self.clients.splice(self.clients.indexOf(clientObject), 1);
        }
    }

    broadcast(originClient, message) {
        console.log(`Broadcasting ${JSON.stringify(message)}`);
        this.WebSocketServer.clients.forEach(function (client) {
            if (client !== originClient) {
                client.send(JSON.stringify(message));
            }
        });
    }
}

module.exports = ServerModel;
